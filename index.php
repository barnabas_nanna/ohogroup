<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Wind Turbine Inspection</title>
</head>
<body>
<div class="container">
    <h1>Wind Turbine Inspection</h1>
    <div class="row">
        <div class="col-6">
            <?php
            $noOfItems = 100;
            $rows = [];
            for ($x = 1; $x <= $noOfItems; $x++) {
                $text = $x;
                if ($x % 3 === 0 && $x % 5 === 0) {
                    $text = 'Coating Damage and Lightning Strike';
                } elseif ($x % 5 === 0) {
                    $text = 'Lightning Strike';
                } elseif ($x % 3 === 0) {
                    $text = 'Coating Damage';
                }
                $rows[] = $text;
            }
            ?>

            <table class="table table-dark table-striped table-hover">
                <tr class="row">
                    <td>
                        <?php echo join('</td></tr><tr class="row"><td>', $rows); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

</body>
</html>
